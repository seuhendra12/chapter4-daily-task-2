function filterCompanyAndEyeColor(arr) {
	// body...

	// Tempat penampungan
	const result = [];

	// Perulangan mencari data perIndex
	for (let i = 0; i < arr.length; i++){

		// Kondisi dimana companynya Pelangi atau intel dan warna matanya hijau
		if (arr[i].company != "FSW4" && arr[i].eyeColor === "green") {
			result.push(arr[i]);
		}
	}

	// Kondisi jika data tidak ditemukan
	if (!result.length) {
		
		// Import file errorHandler
		const error = require('./errorHandler.js');

		// Ngepush errornya
		result.push(error);
		return result;
	}
	return result;
}

// Import data
const data = require('./datas.js');

// Eksport data
module.exports = filterCompanyAndEyeColor(data);