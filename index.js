// load the things we need
var express = require('express');
var app = express();

app.use( express.static( "views" ) );

// set the view engine to ejs
app.set('view engine', 'ejs');

// index page
app.get('/', function(req, res) {
    res.render('pages/index');
});

// about page
app.get('/about', function(req, res) {
    res.render('pages/about');
});

// Import API
const allData = require('./data/datas.js')
const data1 = require('./data/data1.js')
const data2 = require('./data/data2.js')
const data3 = require('./data/data3.js')
const data4 = require('./data/data4.js')
const data5 = require('./data/data5.js')
const cari = require('./search.js')

// Menampilkan data dengan req query 
app.get('/allData', function(req, res) {
    const datas = cari(allData, req.query.search)
    console.log(req.query.search);
    res.render('pages/allData',{datas})
});

// Data 1 page
app.get('/data1', function(req, res) {
    res.render('pages/allData', {
        datas : data1
    });
});

// Data 2 page
app.get('/data2', function(req, res) {
    res.render('pages/allData', {
        datas : data2
    });
});

// Data 3 page
app.get('/data3', function(req, res) {
    res.render('pages/allData', {
        datas : data3
    });
});

// Data 4 page
app.get('/data4', function(req, res) {
    res.render('pages/allData', {
        datas : data4
    });
});

// Data 5 page
app.get('/data5', function(req, res) {
    res.render('pages/allData', {
        datas : data5
    });
});

// Error page
app.use('/', function(req, res) {
    res.status(404);
    res.render('pages/notFound');
});


app.listen(8000);
console.log('8000 is the magic port');